
# Xavier Immersion: Item-bank and computerized adaptive system for stealth immersion evaluation in situational e-learning environments following reproducible research standards

<!-- Joao, reproducible here means that we will insert the R code inside this document --> 

Elias Carvalho, MSc, PhD candidate
Joao Vissoci, PhD candidate
Thomas Vaidhyan, MBA
Adelia Batilana
Ricardo Pietrobon, MD, PhD

<!-- this article is highly technical, which means that once we have it out we will likely have to write a corresponding white paper explaining what it means for educators without a psychometric background. from that paper, then our marketing team led by Thomas, Dorothy and Anu can make sure that the info can get to Deans of Education around the country --> 

## Abstract 
<!-- will write at the end --> 


## Introduction

With the advent of massive open online courses (MOOC), education is now available to a number of individuals around the world. From freely available courses from top universities all the way to affordable courses in niche areas, individuals who were previously left at the margin of society can now have a path towards better education and, with that, improve their odds of a better life. <!-- insert references to support these statements--> Despite a significant improvement in access, the revolution is not without limitations. For example, initial reports demonstrate that only about 10% of all students signing up for a course will complete it [New York Times, 2013](http://www.nytimes.com/2013/02/21/education/universities-abroad-join-mooc-course-projects.html?_r=0). In addition, multiple commentators have explored the lack of engaging activity that would differentiate the current MOOC formats from simply throwing slides, slides, and videos on the Web. In other words, current MOOCs largely lack engagement and immersion, this lack largely resulting from the difficulty in measuring immersion in a way that would not disrupt immersion itself.

Item Response Theory (IRT) assigns difficulty parameters to each of the items (questions), thus making the item an independent measurement unit that adds information to the measurement of an individual's ability [Embretson, 2000](http://books.google.com/books/about/Item_Response_Theory.html?id=rYU7rsi53gQC) This approach is in contrast with classical measurement theory where the unit of measurement is a monolithic scale, or a group of items. The consequences of these different are varied and of significance in the practice of education. First, since the unit of measurement is an item rather than a whole scale, items can be stealthily inserted into an e-learning environment to perform an assessment without disrupting the learning experience. Second, items can be placed within Computerized Adaptive Testing (CAT) environments so that not only the assessment is customized to the ability level of that specific student but also minimizing the number of items to be responded before a conclusion can be reached regarding the actual ability level.<!--refs on CAT -->

The immersion concept has been substantially explored in the gaming literature, although there is a lack of consensus regarding what immersion means and which construct might be involved. For example, constructs have been defined as involving narrative, strategic, tactical, and spatial immersions <!--Adams, Ernest (July 9, 2004). "Postmodernism and the Three Types of Immersion". Gamasutra. Retrieved 2007-12-26.; Björk, Staffan; Jussi Holopainen (2004). Patterns In Game Design. Charles River Media. p. 206. ISBN 1-58450-354-8. -->
, engagement, engrossment and total immersion <!-- A Grounded Investigation of Game Immersion Emily Brown and Paul Cairns --> <!-- add others -->
. The concept of immersion has also been compared to related concepts such as flow <!-- Czsentmihalyi M. Flow: The Psychology of Optimal Experience. Harper Perennial, 1990 --> , attention <!-- Your comment --> 
, and ready-to-hand <!-- Winograd T., Flores F., Understanding Computers and Cognition. Norwood/NJ: Ablex. 1986 -->
. In reality, when taking a bird's eye view on the definition of the immersion construct, it varies substantially not only across individuals, but also over time and across different situations lived by that individual. In other words, a gaming experience by an child will be different from the experience of a teenager the same game, whereas that same teenager could feel a different type of immersion while watching a movie. This situational diversity calls for a "meta-dynamic" assessment that is constantly assessing not only the construct level but also what constitutes the very construct.

<!-- Joao, please add more literature to the Zotero library and then cite it here along with the pieces that you think would be most relevant in the context of this paper -->

When it comes to learning environments, the concept of immersion not only lacks a clear definition and measurement, but it is also relatively new to the field. For example, narrative, strategic and tactical immersion immersion could be applicable if the educational material could be presented in a situational context where students are immersed in a context where their newly acquired abilities are to be used. Spatial immersion would require additional resources within the environment. In addition, other elements of immersion might include elements of fun, where entertainment plays a role in not only understanding the concepts and tools being taught but also ensuring that learners can put them into practice. Immersion in learning environments is mostly an open field.

Given the dynamic, ever changing nature of the concept of immersion in education, and the advantages in using IRT in conjunction with Computerized Adaptive Testing environments, the objective of this study is therefore to describe the creation of an item bank focused on the immersion construct and validated through Item Response Theory. Given its focus on items as a measurement unit rather than classical scales, we also demonstrate that a small number of items provided in a Computerized Adaptive Testing (CAT) environment can be effective in measuring the immersion construct within a timeframe that would not be disruptive in a situational e-learning environment.

## Methods

## Delphi methodology for construct choice
Given the multiple construct descriptions of immersion in the literature, we established a focus group involving a educators and psychometricians to reach consensus on a group of constructs that would provide the starting point for our item bank. 

Consensus was achieved by using a Delphi methodology <!-- add Delphi template --> 


## Item writing
Items were written by four researchers following a standard methodology <!-- add here a standard template for  --> 

### Study sample
We collected data using [Amazon's Mechanical Turk]()<!--get description from existing papers - check Talitha's -->


### Data collection
Mechanical turk <!--get description from existing papers - check Talitha's -->


### Descriptive statistics
We performed a graphical exploratory analysis of all socio-demographic variables, along with the distribution of individual items. Specifically, we looked at 

### Factor analysis
<!-- Joao, please add, but bear in mind that since CAT will use dichotomous items we might use an IRT factor analysis for dichotomous items as well as regular factor analysis for the original Likert scale with 4 item options--> 
<!-- Joao, this section is what central to the "dynamic" nature of the measurement, meaning that we have to find a way to incorporate factor analysis not only as a one-shot thing but as something that will keep iterating over different situations (same person or different people). one way to do this would be to keep collecting data over time along with information on the situations, and once we have a cluster of individuals that is big enough we re-run the factor analysis and redistribute the items across different constructs. notice that this dynamic nature of the items moving across ever changing constructs is the PERFECT justification for multi-dimensional IRT and CAT. in the future, perhaps a CAT could have such a tight integration with the underlying database that it would attempt a semi-automated factor analysis once it reaches a sample size that is big enough for the analysis to be reliable. man, this will be good. no kidding, i am really liking this new role of content researcher, it's piece of cake --> 

### Validation
<!-- Joao, let's add one short scale and then validate not by using short forms but by comparing scores generated through CAT with scores generated through the classical scale of your choice --> 

### IRT modeling
<!-- Joao, please add only unidimensional validation --> 

### Differential item functioning
<!-- Joao, please add. Notice that we will have to choose at least four items that don't have DIF a priori - this is the bullshit portion with DIF evaluation, but let's play along for the time being  --> 

### Computerized adaptive testing

#### Concerto
We used the open source platform [Concerto]() in conjunction with the [catR package]() from the [R]() statistical language <!-- add official reference --> 

#### Item selection algorithm

#### Stopping rule



## Results
<!-- i'm trying to create a regular expression that will grab all headings from the methods and simply paste them into the results, ultimately ensuring that we can't miss anything --> 

## Discussion
### Summary

### Result_1 explanation

### Result_2 explanation

### Result_3 explanation

### Limitations
Despite a significant advance both in the content area of immersion as well as the methodological aspects in measuring a construct that seems to have a wide variance across different situations, our article has limitations. The most important limitation is that this concept should be tested across a wider range of situations, thus testing whether the dynamic nature of the construct indeed changes. These situations could be variations over time for the same individuals as well variations across different situations for the same individuals. Second, although a quantitative evaluation certainly provides a number of insights, we did not perform a qualitative evaluation to further understand the dynamic change in construct. A mixed methods type of design would therefore be warranted in future studies. Last, although we have emphasized the dynamic nature of construct , current CAT system still cannot handle ever changing constructs over time. This tighter integration should be the target of CAT researchers in the future.

### Future research
Our results are encouraging in that, to the best of our knowledge, an item bank has now been developed to measure immersion in a Computerized Adaptive Testing environment. This environment can now be readily used to evaluate immersion in learning environments as well as other environments where immersion is a critical component in use experience.


## References
<!-- can we use BibTeX for markdown?? Elias please google this and then let us know - i'm offline sitting on the piazza erbe in Verona - no way the internet will work in this place despite multiple attempts --> 

<!-- situated? -->

## Appendix - Item list

### Cognitive involvement
#### Positively framed
1. This experience completely held my attention
1. I felt I was completely focused on the learning experience
1. I put a tremendous amount of effort into the learning experience
1. I was trying my very best during the learning experience
1. I found the learning experience to be extremely challenging
1. I felt extremely motivated while having the learning experience
1. I was completely focused on the less
1. Nothing could distract me from the learning experience once I got started
1. I was completely absorbed by the learning experience
1. When I took the learning experience it was like if nothing else existed
1. Nothing would distract me while I was having the learning experience
1. All my attention was concentrated on the learning experience
1. My focus was entirely directed at the learning experience
1. I was completely absorbed in this learning experience
1. I could not think of anything else while having this learning experience
1. While having this learning experience, I could only think about it. 
1. I was completely concentrated while having this learning experience
1. Even if there was an earthquake I would still be completely involved in this learning experience
1. I was paying attention to every single detail while I was having this learning experience
1. My thoughts were completely focused on this learning experience

#### Negatively framed
1. I was very easily distracted while having this learning experience
1. I could not concentrate while having this learning experience
1. I kept taking breaks from time to time in order to be able to focus on this learning experience
1. I could not keep my attention on the tasks I was supposed to do while having this learning experience
1. Even a minor event would get my attention away from this learning experience
1. While having this learning experience my thoughts kept moving away from what I was supposed to do
1. I could barely keep focused on the tasks at hand for this learning experience
1. I was always thinking of an excuse to stop whatever I was doing while having this learning experience
1. I felt like sleeping every time I had to interact with this learning experience
1. I was always thinking about something else while going through this learning experience




### Real world dissociation
#### Positively framed
1. I felt unaware of being in the real world while having the learning experience 
1. I completely forget about my everyday concerns while having the learning experience
1. I wasn't aware of myself in my own surroundings while having the learning experience
1. I did not notice any other events taking place around me while having the learning experience
1. I could not see anything else while having this learning experience
1. I could not hear anything else while having this learning experience
1. People would talk with me and I would not listen while having this learning experience
1. I stopped listening to anything around me while having this learning experience
1. I was completely disconnected from the world surrounding me while having this learning experience
1. It felt like there was nothing other than this learning experience
1. I was completely absorbed into the learning experience, to the point of forgetting where I was
1. It felt like I was in a different world while having this learning experience

#### Negatively framed
1. I was completely aware of my surroundings while going through this learning experience
1. I was never really "into" this learning experience
1. I was felt like I knew where I was while going through this learning experience
1. I was always connected with my surrounding environment while going through this learning experience
1. This learning experience did not make me feel immersed at all
1. I absolutely did not feel like I was absorbed into this learning experience
1. In no moment I felt like I was "into the environment" where this learning experience occurred
1. I could not feel immersed in this learning experience at any moment
1. I was paying attention to every single detail in the place where I was located while having this learning experience
1. I absolutely not absorbed into the environment of my learning experience



### Emotional involvement
#### Positively framed
1. I felt emotionally attached to the learning experience
1. I was absolutely interested in seeing how the events would progress during this learning experience
1. I really wanted to succeed while having this learning experience
1. I was absolutely in suspense about whether or not you would do well while having this learning experience
1. I found myself so involved during this learning experience that I did speak to the characters directly
1. I was in love with this learning experience
1. I felt very strong emotions while having this learning experience
1. Strong emotions completely dominated this learning experience
1. I felt strongly attached to this learning experience
1. It is impossible not to have strong emotions in relation to this learning experience

#### Negatively framed
1. I did not feel a thing while having this learning experience
1. I was pretty apathetic during the whole learning experience
1. I did not like the learning experience at all
1. The emotions within this learning experience were pretty bland
1. I was hardly touched by any of the events within this learning experience
1. I felt emotionally removed while going through this learning experience
1. I could not connect at all with this learning experience
1. I neither liked nor disliked this learning experience, I was simply neutral in relation to it
1. No love, no fear, no hate, I felt nothing in relation to this learning experience
1. This whole learning experience did not bring me any emotional memories that I can remember 



### Control
#### Positively framed
1. I absolutely did not feel the urge at any point to stop the learning experience and see what was happening around me
1. I absolutely felt like I was interacting inside the environment of this learning experience 
1. I found myself so involved that I was unaware that I was using a computer, it was absolutely seamless
1. I felt as though I was moving through the learning experience according to my own will
1. I was in complete control of the environment in my learning experience
1. I felt like I was completely in charge of modifying the environment for my learning experience at any time I wanted
1. I had a major amount of interaction with the environment while having this learning experience
1. I felt very much empowered during my learning experience
1. The interaction during this learning experience kept me with a feeling of absolutely control over the environment
1. I felt like I could modify my learning experience in any way I wanted

#### Negatively framed
1. I felt like an spectator during this whole learning experience, no control at all
1. The learning experience was happening in front of me, but I was not participating
1. I was a bystander in this whole learning experience
1. I felt like I wanted to have more control during this learning experience, but I had none
1. This whole learning experience was like watching TV, in the sense that whatever events were unfolding were completely out of my control
1. I had not control whatsoever about how my learning experience unfolded
1. I felt like the learning experience was external to me, the whole thing happening despite my presence
1. The whole learning experience was completely disconnected from me, in the sense that I could not modify anything if I wanted.
1. I felt powerless while attempting to personally engage with this learning experience
1. The learning experience did not take into consideration that I was a part of it since I had no control to change it.



### Challenge
#### Positively framed
1. There were no times during the learning experience in which I wanted to give up
1. I found the learning experience extremely easy
1. I performed extremely well in this learning experience
1. I was in absolute suspense about whether or not I would do well in this learning experience
1. I felt like I had to overcome the challenges posed by this learning experience at any cost
1. Nothing could be between me and the accomplishment of the goal behind this learning experience
1. I felt extremely challenged by this learning experience
1. The minute I understood what was involved in this learning experience I could not stop thinking about how to overcome the challenge it posed
1. I kept thinking all the time about strategies to overcome the challenges posed by this learning experience
1. While having this learning experience I had this strong feeling that I had to overcome the challenge no matter what

#### Negatively framed
1. The learning experience was too easy, not challenging at all
1. I felt pretty bored with the whole learning experience
1. I didn't feel stimulated at all with the learning experience
1. The material was way under my current ability for this learning experience
1. I did not feel challenged at all during this learning experience
1. The material was too easy for me during this learning experience, I aced it
1. Every portion of the material was very familiar to me, I could do it during my sleep
1. Going through this learning experience did not pick my interest in any way
1. I completely lost interest as I was going through the learning experience
1. I frequently found myself looking at the material from this learning experience while thinking about something completely different