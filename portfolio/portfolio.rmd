# A learning portfolio based on CAT true scores and Complex Learning IDM


Joao Vissoci
Adelia Batilana
Elias Carvalho
Cristiano Gomes
Hudson Golino
Jacson Barros
Jose Eduardo Santana
Ricardo Pietrobon


<!-- http://goo.gl/K4zM9 shiny and concerto -->
<!-- plotting likert scales http://goo.gl/Mimqp -->
[qgraph](http://goo.gl/gZisY)

## Abstract
<!-- will write at the end --> 




## Introduction
The widespread use of item response theory in educational assessment is usually justified by its ability to generate true scores, or scores that are equal to the students' knowledge level. While true scores are inherently valuable in providing students an assessment of their individual skill levels, the representation of whole learning tasks, to the best of our knowledge complex skills with multiple constructs and different proficiency levels has not been explored.  

<!-- lit review on learning tasks - check references from book Merrienboer -->

<!-- lit review on graphical methods to display progression of latent constructs over time -->

The objective of this article is therefore to compare different skill portfolio representations using a mixed methods methodology, and also presenting an application that allows for integration of this display into Web frameworks within learning management systems. 



## Methods

### Portfolio components and data simulation

In order to conduct this experiment, we simulated a series of variables over time for the following Complex Learning constructs: learning tasks, sequencing, ability to find supporting info, procedural info. Table 1 presents an operational definition for each of these constructs. 

Table 1. Evaluated constructs and respective definitions

Construct | Definition
----------|-----------
Learning tasks |  
Sequencing | 
Ability to find supporting information |
Procedural information | 

The simulation assumed a total of 1000 students, with five learning tasks and three sequences or difficulty levels, abilities to find supporting information, and procedural information. In other words, our simulation had 10000*5*3*3 = 450000 data points.

* [simFrame](http://cran.r-project.org/web/packages/simFrame/vignettes/simFrame-intro.pdf)
* [simPopulation](http://cran.r-project.org/web/packages/simPopulation/vignettes/simPopulation-eusilc.pdf)
*[catR](http://goo.gl/fY9lI)

<!-- R: A Language and Environment for Statistical Computing, R Core Team, R Foundation for Statistical Computing, Vienna, Austria, 2013, http://www.R-project.org/ -->



### Portfolio presentation

In order to present the portfolio information we generated graphics using the ggplot2, shiny, [animations](http://goo.gl/fwvDI), and [rcharts](http://goo.gl/OdWyT) packages 

<!-- ref --> from the R language 
<!-- ref -->. The resulting widget was presented at the home page of a learning management system, where students then have the ability to share with specific people, groups, or publicly through email and the following social networks: Twitter, Google+, Facebook and LinkedIn.
<!-- add links to each social network --> All development was deployed through the Django Web framework. 

We generated a total of five portfolio models following a series of design principles as outlined under Table 2.

Principles | Explanation
----------|-----------
 |  

The five portfolio models are presented under Figure 1, with its underlying principles outlined under Table 3.

Portfolio model | Principles
----------------|-----------
  |  




### Mixed methods evaluation

We tested 
<!-- number --> different displays using a mixed methodology. Specifically, we conducted qualitative interviews while exposing each participant to the different types of portfolio presentation (Table 3). Each evaluation was conducted by asking participants regarding a sequence of semi-open questions following a script that was progressively incremented over the course of the study. Table 4 presents the central concepts from this script.

Table 4. Main concepts investigated during the study regarding each portfolio model


<!-- check qualitative template for other models and then create snippet -->
<!-- check graphic on XS for other methods -->




## Results

### Data simulation

#### Simulation of Continuous data

mirt::simdata


Example based on [Stevenson CW, 2013](http://statistical-research.com/simulating-random-multivariate-correlated-data-continuous-variables/?utm_source=rss&utm_medium=rss&utm_campaign=simulating-random-multivariate-correlated-data-continuous-variables) using [Cholesky decomposition](http://en.wikipedia.org/wiki/Cholesky_decomposition)

```{r}
# set correlation matrix
R = matrix(cbind(1,.80,.2,  .80,1,.7,  .2,.7,1),nrow=3)

# apply Cholesky decomposition
U = t(chol(R))

nvars = dim(U)[1]
numobs = 100000
set.seed(13456)
random.normal = matrix(rnorm(nvars*numobs,0,1), nrow=nvars, ncol=numobs);
X = U %*% random.normal
newX = t(X)
raw = as.data.frame(newX)
orig.raw = as.data.frame(t(random.normal))
names(raw) = c("response","predictor1","predictor2")
cor(raw)
plot(head(raw, 100))
plot(head(orig.raw,100))

```


#### Simulation of categorical data

[Wesley, 2013](http://statistical-research.com/simulating-random-multivariate-correlated-data-categorical-variables/?utm_source=rss&utm_medium=rss&utm_campaign=simulating-random-multivariate-correlated-data-categorical-variables)

```{r}
require(GenOrd)
set.seed(3475)
# Sets the marginals.
# The values are cumulative so for the first variable the first marginal will be .1, the second is .2, the third is .3, and the fourth is .4
marginal <- list(c(0.1,0.3,0.6),c(0.4,0.7,0.9))
# Checks the lower and upper bounds of the correlation coefficients.
corrcheck(marginal)
# Sets the correlation coefficients
R <- matrix(c(1,-0.6,-0.6,1),2,2) # Correlation matrix
n <- 100
##Selects and ordinal sample with given correlation R and given marginals.
m <- ordsample(n, marginal, R)
##compare it with the pre-defined R
cor(m)
table(m[,1],m[,2])

chisq.test(m)

gbar <- tapply(m[,1], list(m[,1], m[,2]), length)

par(mfrow=c(1,1))
barplot(gbar, beside=T, col=cm.colors(4), main="Example Bar Chart of Counts by Group",xlab="Group",ylab="Frequency")

```


## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future
* strategy to calibrate each one of our tests against some known widely accepted metric.
create a mechanism to communicate this value to students so that they can understand it and prefer our courses over others
